import '@babel/polyfill';
import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import config from '../config/environment';
import { getToken } from './services/auth.service';
import cron from 'node-cron';

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit : config.bodyLimit
}));

// private token schedule
const crontask = () => {
  cron.schedule(config.cron.privateToken, () => {
    getToken('private', true);
  }).start();
}

// connect to db
initializeDb( db => {
	// connect to private app
	if(config.private){
		getToken('private', true);
		crontask();
	}

	// internal middleware
	app.use(middleware(app));

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
