import config from '../../../config/environment';
import axios from 'axios';
import wrapAsync from '../../services/wrap.async';
import fs from 'fs';
import path from 'path';
import { find, cloneDeep } from 'lodash';
import { applicationTypes } from '../../../config/constant'

let { tokenNames } = config;

export const login = (req, res) => {
  const { clientId, state} = config.apiGateway;
  const redirect_uri = req.query.redirect_uri || config.sso.credentials.redirect_uri;
  return res.status(200).send(`${config.sso.sso}/auth/oauth/login?client_id=${clientId}&state=${state}&redirect_uri=${redirect_uri}`);
}

export const exchangeToken = wrapAsync(async (req, res) => {
  let { code } = req.params;
  let { host, clientId, clientSecret, state } = config.apiGateway;
  let data = {
    client_id: clientId,
    client_secret: clientSecret,
    grant_type: 'authorization_code',
    code, state
  }

  let exchangeTokenRes = await axios.post(`${host}/auth/oauth/access_token`, data);
  let { access_token, refresh_token, expires_in } = exchangeTokenRes.data;
  let cookieOptions = config.cookie;
      cookieOptions.maxAge = expires_in*config.cookieExpiresMultiplier;
  res.cookie(tokenNames.accessToken, access_token, cookieOptions);
  res.cookie(tokenNames.refreshToken, refresh_token, {maxAge: (60*60*24*7)*1000});
  return exchangeTokenRes.data;
});

export const refreshToken = wrapAsync(async (req, res) => {
  let { refreshToken } = req.params;
  let { host, clientId, clientSecret } = config.apiGateway;
  let data = {
    client_id: clientId,
    client_secret: clientSecret,
    grant_type: 'refresh_token',
    refresh_token: refreshToken
  }

  let refreshTokenRes = await axios.post(`${host}/auth/oauth/access_token`, data);
  let { access_token, refresh_token, expires_in } = refreshTokenRes.data;
  let cookieOptions = config.cookie;
      cookieOptions.maxAge = expires_in*config.cookieExpiresMultiplier;
  res.cookie(tokenNames.accessToken, access_token, cookieOptions);
  res.cookie(tokenNames.refreshToken, refresh_token, {maxAge: (60*60*24*7)*1000});
  return refreshTokenRes.data;
});

export const me = wrapAsync(async req => {
  let { host } = config.sso;
  let { clientId } = config.apiGateway;
  let { companyId } = req.params

  let headers = {
    Authorization: req.headers.authorization,
    'x-client': clientId
  }

  if(companyId) headers['x-company-id'] = companyId;  
  let resMe = await axios.get(`${host}/auth/oauth/${config.applicationType}/me`, {
    headers
  });

  let company = find(resMe.data.company, {id: Number(companyId)})
  if(companyId && !company) throw new Error('Company with spesified ID is not found in your profile');
  let menu = fs.readFileSync(path.join(__dirname, `../../../config/menu/${config.applicationType}${company ? '-company' : ''}.json`), 'utf8');
      menu = JSON.parse(menu);

  let resources = (config.applicationType ===  applicationTypes.INTERNAL ? resMe.data.servicePermission : resMe.data.permission).map(p => (
    `${p.method}:${p[config.applicationType ===  applicationTypes.INTERNAL?'clientPath':'resourceUri']}`
  ))

  for(let i = menu.length -1 ; i >= 0 ; i--){
    let menuItem = menu[i];
    if(i === 0 ) menu[i].defaultCollapse = true;
    if(menuItem.children){
      menuItem.children = menuItem.children.filter(function(d){
        d.path = companyId ? d.path.replace(':companyId', companyId) : d.path;
        return d.resourceUri === '' ? true : (resources.indexOf(d.resourceUri) >= 0);
      });
      if(menuItem.children.length == 0) menu.splice(i, 1);
    }
  }
  
  let user = {
    ...resMe.data, 
    companies: cloneDeep(resMe.data.company), 
    menu, company, resources
  }

  const { role } = resMe.data
  if(role && role.name === 'SUPPORT_AGENT'){
    user.isSupport = true
    user.isAdmin = role.isAdmin
  } else {
    user.isSupport = false
  }
  
  return user;
})