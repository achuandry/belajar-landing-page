import { version } from '../../package.json';
import { Router } from 'express';
import sso from './sso';
import env from './env';
import _private from './private';

export default (app) => {
	
	// mount the facets resource
	app.use('/api/sso', sso);
	app.use('/api/env', env);
	app.use('/api/private', _private);

	// perhaps expose some API metadata at the root
	app.use('/api/version', (req, res) => {
		res.json({ version });
	});
}
