
import config from '../../config/environment';
import axios from 'axios';
import compose from 'composable-middleware';
import moment from 'moment';
import _ from 'lodash';
import jwt from 'jsonwebtoken';
import cache from 'memory-cache';

export const getToken = async (name, forceUpdate=false) => {
  let credentials = _.cloneDeep(config[name].credentials);
  let cacheName = `cache-${name}`;
  let cacheRefreshName = `cache-refresh-${name}`;
  let token = cache.get(cacheName);
  let refreshToken = cache.get(cacheRefreshName);
  
  if(token && !forceUpdate){
    return {
      errot: null,
      token: token
    }
  } else {
    console.log(`force update ${name}`)
    try {
      if(refreshToken) {
        credentials.refresh_token = refreshToken;
        credentials.grant_type = 'refresh_token';
      }

      delete credentials.sharedKey

      let result = await axios.post(`${config.sso.host}/auth/oauth/access_token`,credentials);
      let expiresIn = Number(result.data.expires_in);
      expiresIn = (expiresIn > 500 ? expiresIn - 500 : expiresIn)*1000;
      console.log(`Access has granted, ${name} access token is - ${result.data.access_token} expired in ${expiresIn}`)

      cache.put(
        cacheRefreshName,
        result.data.refresh_token,
        60*1000*60*24
      )
      
      cache.put(
        cacheName, 
        result.data.access_token, 
        expiresIn,
        () => {
          return {
            error:null,
            token:result.data.access_token
          }
        }
      )
    } catch (err) {
      console.log(err.response)
      cache.clear();
      if(forceUpdate){
        getToken(name, false);
      } else {
        return {
          error:err,
          token:null
        }
      }
    }
  }
}

export const  generatePajakkuProfile = (user, privateKey) => {
  return new Promise(async (resolve, reject) => {
    try{
      let jwtData = _.clone(config.private.jwt);
      jwtData.exp = moment().add(30,'s').valueOf();
      jwtData.iat = Math.floor(Date.now() / 1000);
      jwtData.sub = user.username
      jwtData.metadata = user;
      const token = jwt.sign(jwtData, privateKey ? privateKey : config.private.credentials.client_secret);
      resolve(token)
    }catch(error){
      reject(error);
    }
  })
}

export const isAuthenticated = (name='private') => {
  return compose()
  .use(async (req,res, next) => {
    if(config.enabledAuth){
      const t = await getToken(name);
      if(t) req.headers.authorization = `Bearer ${t.token}`;
      next();
    } else {
      next();
    }
  })
}

export const decodeProfile = (method) => {
  return compose()
    .use(async (req, res, next) => {
      if(config.enabledAuth){
        let xPajakkuProfile = req.headers['x-profile'];
        if(!xPajakkuProfile) return res.status(401).send('Forbidden. X-Profile is not found')
        try{
          const decoded = await jwt.verify(xPajakkuProfile, config.private.credentials.sharedKey, {
            algorithms:["HS256"]
          });
          req.user = decoded.metadata;
          if(req.body && ['post', 'put'].indexOf(method) >= 0){
            if(method === 'post'){
              req.body.createdBy = req.user.username;
              req.body.dateCreated = new Date();
            };
            if(method === 'put'){
              req.body.updatedBy = req.user.username;
              req.body.updatedAt = new Date();
              for(let key of ['_id', 'publishedBy', 'datePublished', 'reviewed', 'reviewedBy','dateReviewed']){
                delete req.body[key];
              }
            }
          }
         
          next();
        } catch (err) {
          return res.status(500).send(err);
        }
      } else {
        req.body.createdBy = 'disabled-auth';
        req.user = {
          username: 'disabled-auth',
          email: 'disabled@auth.com',
          name: 'Disabled Auth'
        }
        next();
      }
    })
}