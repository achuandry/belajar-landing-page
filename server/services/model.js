export default function(){
  return (data, req) => {
    let result = {};
    const getValue = (key) => {
      let val = data[key];
      if([undefined, null].indexOf(val) < 0) result[key] = val;
    }
    for(let key of arguments){
      if(req && String(req.method).toLowerCase() === 'put' && ['dateCreated', 'createdBy'].indexOf(key) < 0){
        getValue(key);
      } else if(req && String(req.method).toLowerCase() === 'post' && ['dateModified', 'modifiedBy'].indexOf(key) < 0){
        getValue(key);
      } else {
        getValue(key);
      }
    }

    return result;
  }
}