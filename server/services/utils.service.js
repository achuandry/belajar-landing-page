import moment from 'moment'

import { limitation } from '../../config/constant';

export const parseNPWP = (npwp) => {
  let result = String(npwp).split('');
  result.splice(2, 0, '.');
  result.splice(6, 0, '.');
  result.splice(10, 0, '.');
  result.splice(12, 0, '-');
  result.splice(16, 0, '.');
  return result.toString().replace(/,/g,'');
}

export const generatePeriod = req => {
  let { startDate, endDate } = req.query;
  endDate = endDate ? moment(endDate).toDate() : new Date();
  startDate = startDate ? moment(startDate).toDate() : moment(endDate).add(limitation.defaultDiffTime.value, limitation.defaultDiffTime.unit).add(+1, 'day').toDate();

  if(moment(startDate).diff(endDate, limitation.maxTimeUnit.unit) > limitation.maxTimeUnit.value)
    startDate = moment(endDate).add(limitation.maxTimeUnit.value, limitation.maxTimeUnit.unit);

  return {startDate, endDate}
}