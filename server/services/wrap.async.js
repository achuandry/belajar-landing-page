import config from '../../config/environment';
import { applicationTypes } from '../../config/constant';
import apiError from './api-error.service';

const wrap = handler => (req, res) => handler(req, res)
  .then(result => {if(res) res.json(result); else return result})
  .catch(error => { 
    // console.log(error.response ? error.response : error.request);
    if(error.response){
      for(let key of ['www-authenticate']){
        let val = error.response.headers[key];
        if(val) res.setHeader(key, val)
      }
      
      let { status, headers } = error.response;
      if(config.applicationType !== applicationTypes.INTERNAL && Number(status) === 401){
        let authenticate = headers['www-authenticate'];
        
        let a = authenticate.split(',');
        let obj = {};
        for( let d of a){
          let s = d.split('=');
          obj[s[0].replace(/ /g,'-').toLowerCase()] = s[1].replace(/"/g, '');
        }

        if(obj['bearer-error'] === 'access_denied' || (obj.error_description && obj.error_description.match(/X-Profile is invalid/g))){
          return res.status(status).send(`
            <h2>Maaf, Anda belum berlangganan aplikasi ini.</h2>
            <p> Anda dapat berlangganan aplikasi ini dengan menghubungi 
              <a href="https://pajakku.com/contact" target="_blank">tim pajakku</a>
            </p>
          `)
        } else {
          // return res.status(status).send(obj.error_description);
          return sendError(res, status, obj.error_description)
        }
      } else {
        // return res.status(status).json(error.response.data);
        return sendError(res, status, error.response.data)
      }
    }else {
      // return res.status(500).send(error.message);
      return sendError(res, 500, error.message)
    }
  })

const sendError = (res, statusCode, error) => {
  return res 
    ? res.status(statusCode)[typeof error === 'object' ? 'json' : 'send'](error)
    : new apiError(error, statusCode)
}

export default wrap