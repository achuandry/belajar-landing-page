import wrapAsync from "./wrap.async";

export default class Controller{
  constructor(tableName, Model, join){
    this.tableName = tableName
    this.db = null;
    this.socket = null;
    this.Model = Model;
    this.join = join;
    this.config = null;
  }

  initConnection(db, socket, config){
    this.db = db;
    this.socket = socket;
    this.config = config;
  }
}