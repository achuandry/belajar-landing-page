// import knex from "knex";
import config from '../config/environment'

export default callback => {
	let db
	// connect to a database if needed, then pass it to `callback`:
	/*
	let { client, host, user, password, database, pool } = config.dbOptions
	db = knex({
		client,
		connection: { host, user, password, database },
		pool: pool ? {
			min: pool.min,
			max: pool.max,
			acquireTimeout: pool.aquireTimeout,
			afterCreate: function(conn, cb){
				console.log('db-connected')
			}
		} : null
	})
	*/

	callback(db);
}
