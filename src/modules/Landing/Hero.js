import React from 'react'
import './Hero.scss';
import bg from '../../assets/img/bg.png';

const Hero = () => {
  return (
    <div>
        <div className="row-hero__wrapper">
            <div className="column-hero__text">
            <h1 className='intro-text'>E-Commerce Platform</h1>
            <h3 className="brief">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nostrum dolor magnam enim eveniet et numquam, vitae doloremque aut, molestias fugiat consequuntur beatae expedita? Aliquid ad incidunt, nostrum labore tempore rerum consequuntur quis doloribus maxime quae error nesciunt suscipit dolorum nulla. Sit voluptatum repellendus aperiam dolore earum? Inventore, rem, ipsum ad quod necessitatibus aliquam, voluptate ipsa quia minima illum magnam iste odit obcaecati. Repudiandae suscipit beatae impedit odio blanditiis commodi, eum sit consequuntur reiciendis dolores architecto quidem! Temporibus, adipisci molestiae quam similique delectus mollitia dolor harum itaque accusantium quas? Eos iusto consequatur modi quibusdam culpa, excepturi dolore. Illo maiores sed nemo.
            </h3>
            </div>
            <div className="column-hero__img">
                <img src = {bg} className="img__hero"/>
            </div>
        </div>
        {/* <div className="hero-wrapper">
          <div className="hero-copy-container">
            <h1 className="hero-title">E-Commerce Platform</h1>
            <p className="hero-copy">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nostrum dolor magnam enim eveniet et numquam, vitae doloremque aut, molestias fugiat consequuntur beatae expedita? Aliquid ad incidunt, nostrum labore tempore rerum consequuntur quis doloribus maxime quae error nesciunt suscipit dolorum nulla. Sit voluptatum repellendus aperiam dolore earum? Inventore, rem, ipsum ad quod necessitatibus aliquam, voluptate ipsa quia minima illum magnam iste odit obcaecati. Repudiandae suscipit beatae impedit odio blanditiis commodi, eum sit consequuntur reiciendis dolores architecto quidem! Temporibus, adipisci molestiae quam similique delectus mollitia dolor harum itaque accusantium quas? Eos iusto consequatur modi quibusdam culpa, excepturi dolore. Illo maiores sed nemo.</p>
            </div>
          <div className="hero-image">
            <img src = {bg} />  
          </div>

        </div> */}
        
    </div>
  )
}

export default Hero