import React from 'react'

import './LandingPage.scss'
import Hero from './Hero'
import NavBar from './NavBar'

const LandingPage = () => {
  return (
    <div>
        <NavBar />
        <Hero />
    </div>
  )
}

export default LandingPage