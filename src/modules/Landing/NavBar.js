import React from 'react'
import './LandingPage.scss'

const NavBar = () => {
  return (
    <div><section class="navigation">
    <div class="nav-container">
      <div class="brand">
        <a href="/internal">PAJAKKU</a>
      </div>
      <nav>
    <div class="nav-mobile">
      <a id="nav-toggle" href="#!"><span></span></a>
    </div>
    <ul class="nav-list">
      <li><a href="/internal">Home</a></li>
      <li><a href="/internal/dashboard">Dashboard</a></li>
      <li>
        <a href="/internal/form">Form</a>
      </li>
      <li><a href="/internal/details/:tab">Details</a></li>
    </ul>
  </nav>
      </div>
  </section>

    </div>
  )
}

export default NavBar