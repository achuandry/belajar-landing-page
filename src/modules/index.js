import DashboardSample from './DashboardSample'
import DetailsSample from './DetailsSample'
import FormSample from './FormSample'
import TableSample from './TableSample'
import LandingPage from './Landing/LandingPage'

export {
  DashboardSample,
  DetailsSample,
  FormSample,
  TableSample,
  LandingPage
}