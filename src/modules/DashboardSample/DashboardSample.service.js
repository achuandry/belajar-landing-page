import { http } from '../../libs/react-mpk/services'

export const getSummary = (query) => {
  console.log(query)
  return http.get(`/dashboard/summary`)
}

export const getChart = () => (
  http.get(`/dashboard/chart`)
)