import basic from './basic.conf';

export default {
	portal:{
		channelName: '',
		forumUrl: 'https://pajakku.com/forum-categories',
	},
	appConsole:{
    website:{
			label:'Website Sobat Buku',
			url:'https://sobatbuku.com/'
		},
		userAccount:{
			label:'User Account Console',
			url:'https://user.staging.sobatpajak.com'
		},
    developer:{
			label:'Developer Console',
			url:'https://developer.staging.sobatpajak.com'
		}
  },
	widgetGateway: {
		host: 'https://widget.staging.sobatpajak.com',
		ws: 'wss://widget.staging.sobatpajak.com',
	},
	apiGateway: {
		internal: {
			host: 'https://internal.staging.sobatpajak.com',
			clientId: '58NDU9b3tJUSs74vbE6FTJ5ROgWYB94xMwWBlxib',
			clientSecret: 'ByAybedLET72iBrJF1fXNCFkv9ZJWgE97nv1JC7T',
			state: 'sobat-buku-web-dev',
			baseUrl: '',
		},
		product: {
			host: 'https://product.staging.sobatpajak.com',
			clientId: '8qKOfRjjsYIyzZHmxY8Va4PxpZw5XTHGMnqczkEz',
			clientSecret: 'I5akCvRmgrxFjAjDprjMMoSWfQGbuuqFK4rb1le3',
			state: 'sobat-buku-web-dev',
			baseUrl: ''
		}
	},
  sso: {
		host: 'https://api.staging.sobatpajak.com',
		sso: 'https://sso.staging.sobatpajak.com',
	},
	widgetInterface:{
		kbs: {url: 'https://kbs.pajakku.com/static/libs/kbs-widget.min.js'}
	},
	documentStatusPkp:{documentURL:"https://api.staging.sobatpajak.com"},
	/*
	private:{
    host:'https://private.staging.sobatpajak.com',
    hostName:'private.staging.sobatpajak.com',
		baseUrl: '/base_url',
    credentials:{
      client_id:'dQ1eg4jbZjO8xuH2pCE4czxbULqn9zX9myFWzKty',
      client_secret:'EmIE3X0AqncNuqRQt8LaRPa2Vkr5cXVAEZbohXBw',
      grant_type:'client_credentials'
    },
		jwt:{
      iss: 'E-Filing 1770 Backend',
      aud: ['Efiling Service'],
      jti: 'def826b5-43df-4d76-8a0c-fb23dbf33e23',
    }
	}
	*/
	kbs:[{label: "kbs.manualGuide", type: "documentation", code: 'DOC-SOBAT-BUKU-WEB', translate: true}],
	path:{
		upload:'./tmp'
	}
}