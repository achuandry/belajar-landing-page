import path from 'path';
import { themes, applicationTypes } from '../constant';

export default {
  env: process.env.NODE_ENV,
  root: path.normalize(__dirname + '/../..'),
  corsHeaders: ['x-pagination-count'],
  enabledAuth: true,

  cookieExpiresMultiplier:900,
  cookie:{
    path:'/',
    secure:false,
    httpOnly:false
  },

  cron: {
    privateToken: '*/1 * * * *'
  },

  locale: {
    code: 'id',
    options: [
      {label: 'Bahasa', value: 'id'},
      {label: 'English', value: 'en'}
    ]
  },

  theme: themes.SOBATPAJAK,
  applicationType: process.env.APPLICATION_TYPE || applicationTypes.INTERNAL
}