import _ from 'lodash';
import conf from './basic.conf';

const mode = process.env.NODE_ENV;
const tokenNames = {
  pajakku: {
    accessToken: 'MPK_ACCESS_TOKEN',
    refreshToken: 'MPK_REFRESH_TOKEN'
  },
  sobatpajak: {
    accessToken: 'SP_ACCESS_TOKEN',
    refreshToken: 'SP_REFRESH_TOKEN'
  }
}

let env = require(`./${mode}.${conf.theme}.js` || {}).default;
for(let key of ['apiGateway']){
  env[key] = env[key][conf.applicationType]
}


let all = _.merge(
  conf,env,
  {tokenNames: tokenNames[conf.theme]}
)

export default all;