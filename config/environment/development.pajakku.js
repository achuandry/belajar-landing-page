import basic from './basic.conf';

export default {
	portal:{
		channelName: '',
		forumUrl: 'https://pajakku.com/forum-categories',
	},
	appConsole:{
    website:{
			label:'Website Pajakku',
			url:'https://portal.bdg.pajakku.com'
		},
		userAccount:{
			label:'User Account Console',
			url:'https://user.bdg.pajakku.com'
		},
    developer:{
			label:'Developer Console',
			url:'https://developer.bdg.pajakku.com'
		}
  },
	widgetGateway: {
		host: 'https://widget.bdg.pajakku.com',
		ws: 'wss://widget.bdg.pajakku.com',
		urlAccessClientId: '',
		baseUrl:''
	},
	apiGateway: {
		internal: {
			host: 'https://internal.bdg.pajakku.com',
			clientId: 'HdW2CQufsuPeF8MbkYEklwZwgzR5xzMYP5I345JV',
			clientSecret: 'kOOSVNDxh7gQO0GKgKqNUQIQnvNqb2hLdGHHkuJD',
			state: 'mpk-starter-spa-internal',
			baseUrl: '',
		},
		product: {
			host: 'https://product.bdg.pajakku.com',
			clientId: '8qKOfRjjsYIyzZHmxY8Va4PxpZw5XTHGMnqczkEz',
			clientSecret: 'I5akCvRmgrxFjAjDprjMMoSWfQGbuuqFK4rb1le3',
			state: 'mpk-starter-spa-product',
			baseUrl: ''
		}
	},
  sso: {
		host: 'https://api.bdg.pajakku.com',
		sso: 'http://sso.bdg.pajakku.com',
	},
	widgetInterface:{
		kbs: {url: 'https://kbs.pajakku.com/static/libs/kbs-widget.min.js'}
	},
	/*
	private:{
    host:'https://private.bdg.pajakku.com',
    hostName:'private.bdg.pajakku.com',
		baseUrl: '/base_url',
    credentials:{
      client_id:'dQ1eg4jbZjO8xuH2pCE4czxbULqn9zX9myFWzKty',
      client_secret:'EmIE3X0AqncNuqRQt8LaRPa2Vkr5cXVAEZbohXBw',
      grant_type:'client_credentials'
    },
		jwt:{
      iss: 'E-Filing 1770 Backend',
      aud: ['Efiling Service'],
      jti: 'def826b5-43df-4d76-8a0c-fb23dbf33e23',
    }
	}
	*/
	kbs:[{label: "kbs.manualGuide", type: "documentation", code: 'DOC-STARTER-SPA', translate: true}],
	path:{
		upload:'./tmp'
	},
	/*
	dbOptions:{
		client: 'mysql',
		host: '127.0.0.1',
		connectionLimit: 100,
		user: 'root',
		password: '',
		database: '',
		debug: false
	},
	path: {
		root: '', //process.execPath,
		xls: './tmp/xls'
	},
	*/
}