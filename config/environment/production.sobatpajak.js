import basic from './basic.conf';

export default {
	// Server IP
	ip:       process.env.OPENSHIFT_NODEJS_IP ||
			process.env.IP ||
			undefined,

	// Server port
	port:     process.env.OPENSHIFT_NODEJS_PORT ||
			process.env.PORT ||
			8085,
	
	portal:{
		channelName: '',
		forumUrl: 'https://sobatbuku.com/forum-categories',
	},
	appConsole:{
	website:{
			label:'Website Sobat Buku',
			url:'https://sobatbuku.com/'
		},
		userAccount:{
			label:"User Account Console",
			url:"https://user.sobatbuku.com"
		},
	developer:{
			label:"Developer Console",
			url:"https://developer.sobatbuku.com"
		}
	},
	widgetGateway: {
		host: "https://widget.sobatbuku.com",
		ws: "wss://widget.sobatbuku.com",
	},
	apiGateway: {
		internal: {
			host: 'https://internal.sobatbuku.com',
			clientId: 'aLjQjmPyY2qxh07q7Y92ZOvovamsc5QcSKNr0jmy',
			clientSecret: 'ZFN2RaO8Qkv0Ku3TCEO95P91Twaf7p6RC3zHjXkK',
			state: 'sobat-buku-web-dev',
			baseUrl: '',
		},
		product: {
			host: 'https://product.sobatbuku.com',
			clientId: '8qKOfRjjsYIyzZHmxY8Va4PxpZw5XTHGMnqczkEz',
			clientSecret: 'I5akCvRmgrxFjAjDprjMMoSWfQGbuuqFK4rb1le3',
			state: 'sobat-buku-web-dev',
			baseUrl: ''
		}
	},
	sso: {
		host: 'https://api.sobatbuku.com',
		sso: 'http://sso.sobatbuku.com',
	},
	widgetInterface:{
		kbs: {url: 'https://kbs.sobatbuku.com/static/libs/kbs-widget.min.js'}
	},
	documentStatusPkp:{documentURL:"https://api.staging.sobatpajak.com"},
	/*
	private:{
	host:'https://private.sobatbuku.com',
	hostName:'private.sobatbuku.com',
		baseUrl: '/base_url',
	credentials:{
		client_id:'dQ1eg4jbZjO8xuH2pCE4czxbULqn9zX9myFWzKty',
		client_secret:'EmIE3X0AqncNuqRQt8LaRPa2Vkr5cXVAEZbohXBw',
		grant_type:'client_credentials'
	},
		jwt:{
		iss: 'E-Filing 1770 Backend',
		aud: ['Efiling Service'],
		jti: 'def826b5-43df-4d76-8a0c-fb23dbf33e23',
	}
	}
	*/
	kbs:[{label: "kbs.manualGuide", type: "documentation", code: 'DOC-SOBAT-BUKU-WEB', translate: true}],
	path:{
		upload:'./tmp'
	}
}