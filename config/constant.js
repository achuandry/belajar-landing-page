export const themes = {
  PAJAKKU     : 'pajakku',
  SOBATPAJAK  : 'sobatpajak'
}

export const applicationTypes = {
  INTERNAL    : 'internal',
  PRODUCT     : 'product',
  GENERAL     : 'general'
}